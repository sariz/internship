$(document).ready(function () {
  goto('animation');
});

function goto(page) {
  $('.main-container').fadeOut(400, function () {
    $('.main-container').load('pages/' + page + '.html', function () {
      $.getScript('js/pages/' + page + '.js', function () {
        $('.main-container').fadeIn(400);
      });
    });
  });
}
