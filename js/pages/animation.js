$(document).ready(function () {
  animationHandle();

  btnHandle();
});

function btnHandle() {
  $('#btn-nextpage').one('click', function () {
    goto('interaction');
  });
  // btn-nextpage
}

function animationHandle() {
  // selector
  var btnHide = $('.btn-hide');
  var btnShow = $('.btn-show');

  // hide/show
  //   btnHide.on('click', function () {
  //     $('.card').hide();
  //   });

  //   btnShow.on('click', function () {
  //     $('.card').show();
  //   });

  // fadeIn/fadeOut
  //   btnHide.on('click', function () {
  //     $('.card').fadeOut();
  //   });

  //   btnShow.on('click', function () {
  //     $('.card').fadeIn();
  //   });

  // slideUp/slideDown
  btnHide.on('click', function () {
    $('.card').slideUp();
  });

  btnShow.on('click', function () {
    $('.card').slideDown();
  });

  // =============================

  // this selector
  //   $('.card').on('click', function () {
  //     $(this).slideUp();
  //   });

  // =============================

  // animation
  $('.card').on('click', function () {
    if ($(this).hasClass('opened')) {
      // ปิด
      $(this).removeClass('opened');
      $(this).animate({ height: '20px' });
    } else {
      $('.card').removeClass('opened');

      // เปิด
      $(this).addClass('opened');
      $(this).animate({ height: '350px' });

      // ปิดทั้งหมดที่ไม่ใช่ตัวที่เลือก opened
      $('.card:not(.opened)').animate({ height: '20px' });
    }
  });
}
