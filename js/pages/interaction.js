$(document).ready(function () {
  console.log('interaction page is ready');

  gameHandle();
});

function gameHandle() {
  $('.box').draggable({
    revert: true,
  });

  $('.drop-box').droppable({
    drop: function (event, ui) {
      var isCorrectAnswer = ui.draggable.data('correct');

      var correctText = 'You have choosen the right cat!';
      var incorrectText = 'This is not your cat, try another one';
      var imgAttr = ui.draggable.html();

      $('.img').html(imgAttr);

      if (isCorrectAnswer) {
        $('.sentence').html(correctText);
      } else {
        $('.sentence').html(incorrectText);
      }
    },
  });
}
