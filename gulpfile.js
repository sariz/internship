// import gulp from 'gulp';
const gulp = require('gulp');

// import scss complie
const sass = require('gulp-sass');
const autoPrefixer = require('autoprefixer');
const postcss = require('gulp-postcss');
const wait = require('gulp-wait');
const cssNano = require('cssnano');
const sourceMaps = require('gulp-sourcemaps');

const concat = require('gulp-concat');
const uglify = require('gulp-uglify');

// conpile แปลง scss เป็น css ให้ html รองรับ
gulp.task('watch', function (done) {
  // watch scss
  gulp.watch(['scss/**/*.scss'], function (cb) {
    minifyScss();
    cb();
  });

  // watch js
  gulp.watch(['js/controller/*.js'], function (cb) {
    minifyJs();
    cb();
  });

  done();
});

function minifyScss() {
  gulp
    .src('scss/style.scss')
    .pipe(sourceMaps.init())
    .pipe(sass())
    .pipe(postcss([autoPrefixer(), cssNano()]))
    .pipe(sourceMaps.write())
    .pipe(gulp.dest('css'));
}

// conpile แปลง .js ให้เป็น script เดียว
function minifyJs() {
  gulp.src(['js/controller/*.js']).pipe(concat('scripts.js')).pipe(uglify()).pipe(gulp.dest('js'));
}
